import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-greeting',
  templateUrl: './profile-greeting.component.html',
  styleUrls: ['./profile-greeting.component.scss']
})
export class ProfileGreetingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
