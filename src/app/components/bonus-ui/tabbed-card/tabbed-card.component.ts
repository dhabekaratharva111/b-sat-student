import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabbed-card',
  templateUrl: './tabbed-card.component.html',
  styleUrls: ['./tabbed-card.component.scss']
})
export class TabbedCardComponent implements OnInit {
  active = 1;

  materialSuccess = "success"
  materialSecondary = "secondary"
  constructor() { }

  ngOnInit(): void {
  }

}
