import {
  Component,
  ElementRef,
  HostListener,
  NgZone,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService, ModalOptions } from "ngx-bootstrap/modal";
import { HttpService } from "src/app/shared/services/http/http.service";
import { BSatEnrollnmentComponent } from "../result-modal/b-sat-enrollnment.component";
import { Location } from "@angular/common";
import { Observable, interval } from "rxjs";
import { CanComponentDeactivate } from "src/app/shared/guard/reload.guard";
import { ReloadPreventionService } from "src/app/shared/services/reload/reload.service";
import { LoaderService } from "src/app/shared/services/loader.service";

const LETTERS = ["A", "B", "C", "D", "E", "F"];

@Component({
  selector: "app-mock-test",
  templateUrl: "./mock-test.component.html",
  styleUrls: ["./mock-test.component.scss"],
})
export class MockTestComponent implements CanComponentDeactivate {
  optionLetters = LETTERS;
  selectedTestId: any;
  questions: any[] = []; // Update the type here
  selectedOptionIndex: number | null = null; // Change the name here
  testId: string = "";
  getResultById: any;
  questionListByTest: any = [];
  selectedQuestionIndex: any = 0;
  selectedQuestion: any;
  modalRef?: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false, // Set to false to disable closing with the "Escape" key
  };
  receivedData: any;
  testDuration: any;
  remainingTime: number;
  formattedRemainingTime: string;
  timerInterval: any;
  @ViewChild("resulttemplate", { static: true })
  resulttemplate: TemplateRef<any>;
  hasUnsavedChanges: any = true;
  message?: string;
  @ViewChild("buttonContainer") buttonContainer: ElementRef;
  mockTestDetails: any;

  constructor(
    private location: Location,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute,
    private httpService: HttpService,
    private ngZone: NgZone,
    private reloadPreventionService: ReloadPreventionService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.reloadPreventionService.preventPageReload(true);

    this.route.queryParams.subscribe((params) => {
      this.testId = params["testID"];
      this.testDuration = params["testTime"];
      console.log("test", this.testDuration);
      if (this.testId) {
        this.loadMockTestQuestionsById();
        this.loadMockTestById()
      }
    });

    this.remainingTime = this.testDuration * 60;
    this.startTimer();
  }

  ngAfterViewChecked(): void {
    // After the view has been checked, scroll to the selected button
    // if (this.selectedQuestionIndex !== null) {
    //   this.scrollSelectedButtonIntoView(this.selectedQuestionIndex);
    // }
  }

  openModal(submitTemplate: TemplateRef<void>) {
    this.modalRef = this.modalService.show(submitTemplate, this.config);
  }

  decline(): void {
    this.modalRef?.hide();
  }

  loadMockTestById() {
    this.loaderService.startLoader();
    this.httpService
      .getById("mockTest/getMockTestById", this.testId)
      .subscribe(
        (response) => {
          this.mockTestDetails = response.data;
          console.log('mockTestDetails', this.mockTestDetails);
          this.loaderService.stopLoader();
        },
        (error) => {
          console.error("API Error:", error);
        }
      );
  }

  loadMockTestQuestionsById() {
    this.loaderService.startLoader();
    this.httpService
      .getById("mockTestQuestions/getMockTestQuestionByMockTestId", this.testId)
      .subscribe(
        (response) => {
          this.questionListByTest = response.data;
          this.onSelectQuestion(
            this.questionListByTest[this.selectedQuestionIndex],
            this.selectedQuestionIndex
          );
          // this.iAmNew =false;
          this.loaderService.stopLoader(); 
        },
        (error) => {
          console.error("API Error:", error);
        }
      );
  }

  onSelectQuestion(question: object, index: any) {
    this.selectedQuestion = question;
    this.selectedQuestionIndex = index;
    this.scrollSelectedButtonIntoView(index);
  }

  onSelectAnswer(optionId: any) {
    const selectedQuestionId = this.selectedQuestion._id;

    const selectedQuestion = this.questionListByTest.find(
      (que: any) => que._id === selectedQuestionId
    );

    if (selectedQuestion) {
      selectedQuestion.answerId = optionId;

      selectedQuestion.optionList.forEach((option: any) => {
        option.isSelected = option.optionId === optionId;
      });
    }
  }

  onClickNext() {
    this.selectedQuestionIndex = this.selectedQuestionIndex + 1;
    this.onSelectQuestion(
      this.questionListByTest[this.selectedQuestionIndex],
      this.selectedQuestionIndex
    );
  }

  onClickPrev() {
    this.selectedQuestionIndex = this.selectedQuestionIndex - 1;
    this.onSelectQuestion(
      this.questionListByTest[this.selectedQuestionIndex],
      this.selectedQuestionIndex
    );
  }

  onSubmitTest(resulttemplate: TemplateRef<void>) {
    this.loaderService.startLoader();
    let sendData = {
      mockTestId: this.questionListByTest[0].mockTestId,
      userId: this.httpService.getUserId(),
      answerSheet: this.getFormatedAnswer(),
    };

    this.httpService.post("submitmockTest/submitMockTest", sendData).subscribe(
      (res) => {
        this.loaderService.stopLoader();
        this.receivedData = res.data;
        this.openResultModal(resulttemplate);
      },
      (error) => {
        this.loaderService.stopLoader();
        console.log("error", error);
      }
    );
  }

  getFormatedAnswer() {
    return this.questionListByTest
      .filter(
        (element: any) =>
          element.answerId !== undefined && element.answerId !== null
      )
      .map((element: any) => ({
        questionId: element._id,
        answerId: element.answerId,
      }));
  }

  openResultModal(resulttemplate: any) {
    this.modalService.show(resulttemplate, this.config);
  }

  okClick() {
    this.modalRef?.hide();
    this.location.back();
    this.hasUnsavedChanges = false;
    this.reloadPreventionService.preventPageReload(false);
  }

  startTimer() {
    this.timerInterval = setInterval(() => {
      this.remainingTime--;
      this.formattedRemainingTime = this.formatTimeDuration(this.remainingTime);
      if (this.remainingTime <= 0) {
        clearInterval(this.timerInterval);
        this.onSubmitTest(this.resulttemplate);
      }
    }, 1000);
  }

  private formatTimeDuration(durationInSeconds: number): string {
    const hours = Math.floor(durationInSeconds / 3600);
    const minutes = Math.floor((durationInSeconds % 3600) / 60);
    const seconds = durationInSeconds % 60;

    return `${this.padZero(hours)} : ${this.padZero(minutes)} : ${this.padZero(
      seconds
    )}`;
  }

  private padZero(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  canDeactivate(): boolean {
    if (this.hasUnsavedChanges) {
      return window.confirm("Do you really want to leave?");
    } else {
      return true;
    }
  }

  private scrollSelectedButtonIntoView(index: number): void {
    const buttons = this.buttonContainer.nativeElement.querySelectorAll(".btn");
    const selectedButton = buttons[index];

    if (selectedButton) {
      selectedButton.scrollIntoView({
        behavior: "smooth",
        block: "nearest",
        inline: "start",
      });
    }
  }
}
