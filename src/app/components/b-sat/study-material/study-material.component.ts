import { Component, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { Checkout } from 'capacitor-razorpay';
import { Subscription, interval } from 'rxjs';
import { LoaderService } from 'src/app/shared/services/loader.service';
declare var Razorpay: any;


@Component({
  selector: 'app-study-material',
  templateUrl: './study-material.component.html',
  styleUrls: ['./study-material.component.scss']
})
export class StudyMaterialComponent {
  @ViewChild('purchaseModalTemplate', { static: true }) purchaseModalTemplate!: TemplateRef<any>;
  classes = CommonConstants.CLASSES;
  materialFee: number;
  selectedClassId: any;
  getSubjectByMaterial: any;

  orderUserDetails = {
    name: 'Learn & Achiveve',
    description: 'Great offers',
    logo: 'https://ibb.co/1qBBz3V',
    prefill: {
      email: 'utkarsha.ioweb3@gmail.com',
      contact: '8308537209'
    }
  }
  orderCreatedData: any;
  purchaseModalRef?: BsModalRef;
  userId: any;
  materialId: any;
  myClass: any;
  myMedium: any;
  studyMaterial: any = [];
  selectedDataToView: any;
  expandStudyMaterial=false;

  constructor(private fb: FormBuilder, private httpService: HttpService,
    private loaderService : LoaderService,
     private modalService: BsModalService, private router: Router) { }

  ngOnInit() {
    this.userId = this.httpService.getUserId();
    this.myClass = this.httpService.getMyClass();
    this.myMedium = this.httpService.getMyMedium();
    this.loadSubjectsForClass();
    this.userId = this.httpService.getUserId();
    this.myClass = this.httpService.getMyClass();
    this.myMedium = this.httpService.getMyMedium();
    this.loadPurchasedMaterialById();
  }

  loadSubjectsForClass() {
    this.loaderService.startLoader();
    this.httpService.getById(`purchaseStudyMaterial/getPurchaseStudyMaterialByClassId/${this.myClass}/${this.userId}`, this.myMedium).subscribe(
      (response) => {
        this.getSubjectByMaterial = response.data ? response.data : {};
        console.log('Received Materials:', this.getSubjectByMaterial);
        this.selectedClassId = response?.data?.classId;
        console.log('this.selectedClassId', this.selectedClassId)
        this.materialFee = response?.data?.materialFee;
        console.log('this.materialFee', this.materialFee);
        this.materialId = response?.data?._id;
        console.log('this.materialId', this.materialId);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      (error) => {
        console.error('API Error:', error);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      }
    );
  }

  proceedPayment() {
    const materialFee = this.getSubjectByMaterial[0]?.materialFee || 0;
    let sendData = {
      amount: this.getSubjectByMaterial.materialFee,
      currency: "INR",
      receipt: "order_receipt",
    };

    this.httpService.post("purchaseStudyMeterialPaymemnt/proceedPayment", sendData).subscribe({
      next: async (response) => {
        // let confirmed: any = await this.alertService.showConfirmation('Subscribe Now!', `You are paying ${response.currency} ${response.amount / 100}.`, 'No', 'Pay Now !');
        // if (confirmed) {
        this.payWithRazorpay(response)
        // }
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  // Step 2
  async payWithRazorpay(res: any) {
    const options = {
      key: 'rzp_test_ZkWbOZ5oCWVi6l',
      amount: res.amount,
      description: this.orderUserDetails.description,
      image: this.orderUserDetails.logo,
      order_id: res.id, //Order ID generated in Step 1
      currency: res.currency, //currency generated in Step 1
      name: this.orderUserDetails.name,
      prefill: {
        email: this.orderUserDetails.prefill.email,
        contact: this.orderUserDetails.prefill.contact
      },
      theme: {
        color: '#0029F5'
      }
    }

    try {
      let razorpaySuccessRes = (await Checkout.open(options));
      this.proceedToCreateOrder(razorpaySuccessRes.response);
      // Step 3 to create entry of order on success
    } catch (error: any) {
      console.log('######### Step:2 Error', error)
      // Step 3 to create entry of order on failure
    }
  }

  // step3

  proceedToCreateOrder(razorpaySuccessRes: any) {
    let sendData = {
      razorpayResData: razorpaySuccessRes,
      paymentStatus: 'paid',
      orderStatus: 'Pending',
      totalAmount: this.materialFee,
      userId: this.httpService.getUserId(),
      materialFee: this.materialFee,
      materialId: this.materialId,

    }
    this.httpService.post("purchaseStudyMeterialPaymemnt/createPurchaseStudyMeterialPaymemnt", sendData).subscribe({
      next: async (response) => {
        this.orderCreatedData = response
        console.log('this.orderCreatedData', this.orderCreatedData);
        this.loadSubjectsForClass();
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  loadPurchasedMaterialById() {
    this.loaderService.startLoader();
    this.httpService
      .getById(
        `studyMaterial/getStudyMaterialByClassId/${this.myClass}`,
        this.myMedium
      )
      .subscribe(
        (response) => {
          this.studyMaterial = response.data;
          console.log("this.studyMaterial", this.studyMaterial);
          this.loaderService.stopLoader();
        },
        (error) => {
          console.error("API Error:", error);
        }
      );
  }

  openModalToShowDetailsData(event: Event, item: any) {
    event.preventDefault();
    this.selectedDataToView = item?.studyMaterial;
    this.expandStudyMaterial=true;
  }

  closeModal() {
    this.expandStudyMaterial=false;
  }
}
