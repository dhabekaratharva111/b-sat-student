import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Checkout } from 'capacitor-razorpay';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

@Component({
  selector: 'app-mock-test-page',
  templateUrl: './mock-test-page.component.html',
  styleUrls: ['./mock-test-page.component.scss']
})
export class MockTestPageComponent {
  modalRef?: BsModalRef;
  config = {
    // backdrop: true,
    // ignoreBackdropClick: true,
    // keyboard: false, // Set to false to disable closing with the "Escape" key
  };
  selectedClassId: any;
  studyMaterial: any = [];
  userId: any;
  mockTest: any[] = [];
  myClass: any;
  myMedium: any;
  getSubjectByMaterial: any;
  selectedDataToView: any;
  isModalOpen = false;
  orderUserDetails = {
    name: 'Learn & Achiveve',
    description: 'Great offers',
    logo: 'https://ibb.co/1qBBz3V',
    prefill: {
      email: 'utkarsha.ioweb3@gmail.com',
      contact: '8308537209'
    }
  }
  orderCreatedData: any;
  materialId: any;
  materialFee: number;
  selectedMockTestResultData: any;

  constructor(private route: ActivatedRoute, private httpService: HttpService, private router: Router, private loaderService: LoaderService,
    private modalService: BsModalService
  ) {
    this.route.params.subscribe(params => {
      this.selectedClassId = params['classId'];
      console.log('Selected Class ID in MockTestPageComponent:', this.selectedClassId);
    });
    this.route.queryParams.subscribe(queryParams => {
      console.log('Query Parameters:', queryParams);
    });
  }

  ngOnInit() {
    this.userId = this.httpService.getUserId();
    this.myClass = this.httpService.getMyClass();
    this.myMedium = this.httpService.getMyMedium();
    this.loadMockTest();
    this.loadSubjectsForClass();
  }

  loadMockTest() {
    this.loaderService.startLoader();
    this.httpService.getById(`mockTest/getMockTestByClassId/${this.myClass}/${this.userId}`, this.myMedium).subscribe(
      (response) => {
        this.mockTest = response.data;
        console.log('this.mockTest', this.mockTest);
        this.loaderService.stopLoader();
      },
      (error) => {
        console.error('API Error:', error);
      }
    );
  }

  loadSubjectsForClass() {
    this.loaderService.startLoader();
    this.httpService.getById(`purchaseStudyMaterial/getPurchaseStudyMaterialByClassId/${this.myClass}/${this.userId}`, this.myMedium).subscribe(
      (response) => {
        this.getSubjectByMaterial = response.data ? response.data : {};
        console.log('Received Materials:', this.getSubjectByMaterial);
        this.selectedClassId = response?.data?.classId;
        console.log('this.selectedClassId', this.selectedClassId)
        this.materialFee = response?.data?.materialFee;
        console.log('this.materialFee', this.materialFee);
        this.materialId = response?.data?._id;
        console.log('this.materialId', this.materialId);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      },
      (error) => {
        console.error('API Error:', error);
        setTimeout(() => {
          this.loaderService.stopLoader();
        }, 200);
      }
    );
  }

  proceedPayment() {
    const materialFee = this.getSubjectByMaterial[0]?.materialFee || 0;
    let sendData = {
      amount: this.getSubjectByMaterial.materialFee,
      currency: "INR",
      receipt: "order_receipt",
    };

    this.httpService.post("purchaseStudyMeterialPaymemnt/proceedPayment", sendData).subscribe({
      next: async (response) => {
        // let confirmed: any = await this.alertService.showConfirmation('Subscribe Now!', `You are paying ${response.currency} ${response.amount / 100}.`, 'No', 'Pay Now !');
        // if (confirmed) {
        this.payWithRazorpay(response)
        // }
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  // Step 2
  async payWithRazorpay(res: any) {
    const options = {
      key: 'rzp_test_ZkWbOZ5oCWVi6l',
      amount: res.amount,
      description: this.orderUserDetails.description,
      image: this.orderUserDetails.logo,
      order_id: res.id, //Order ID generated in Step 1
      currency: res.currency, //currency generated in Step 1
      name: this.orderUserDetails.name,
      prefill: {
        email: this.orderUserDetails.prefill.email,
        contact: this.orderUserDetails.prefill.contact
      },
      theme: {
        color: '#0029F5'
      }
    }

    try {
      let razorpaySuccessRes = (await Checkout.open(options));
      this.proceedToCreateOrder(razorpaySuccessRes.response);
      // Step 3 to create entry of order on success
    } catch (error: any) {
      console.log('######### Step:2 Error', error)
      // Step 3 to create entry of order on failure
    }
  }

  // step3

  proceedToCreateOrder(razorpaySuccessRes: any) {
    let sendData = {
      razorpayResData: razorpaySuccessRes,
      paymentStatus: 'paid',
      orderStatus: 'Pending',
      totalAmount: this.materialFee,
      userId: this.httpService.getUserId(),
      materialFee: this.materialFee,
      materialId: this.materialId,

    }
    this.httpService.post("purchaseStudyMeterialPaymemnt/createPurchaseStudyMeterialPaymemnt", sendData).subscribe({
      next: async (response) => {
        this.orderCreatedData = response
        console.log('this.orderCreatedData', this.orderCreatedData);
        this.loadSubjectsForClass();
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  navigateToMock(testNumber: any) {
    console.log('testNumber', testNumber);
    const navigationExtras: NavigationExtras = {
      state: {
        testID: testNumber._id,
        testTime: testNumber.testTimeDuration
      }
    };

    // Navigate to another component with data
    this.router.navigate(['/b-sat/mock-test'], {
      state: {
        testID: testNumber._id,
        testTime: testNumber.testTimeDuration
      },
      queryParams: {
        testID: testNumber._id,
        testTime: testNumber.testTimeDuration
      }
    });
  }

  calculatePercentage(resultDetails: any): number {
    if (resultDetails && resultDetails.totalMarks > 0) {
      return (resultDetails.obtainedMarks / resultDetails.totalMarks) * 100;
    } else {
      return 0;
    }
  }

  openModalToShowDetailsData(event: Event, item: any) {
    event.preventDefault();
    this.selectedDataToView = item?.studyMaterial;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  seeResultDetails(resulttemplate: TemplateRef<void>, resultDetails: any) {
    this.selectedMockTestResultData = resultDetails;
    this.modalRef =  this.modalService.show(resulttemplate, this.config);
  }

  okClick() {
    this.modalRef?.hide();
    // this.location.back();
    // this.hasUnsavedChanges = false;
    // this.reloadPreventionService.preventPageReload(false);
  }

}
