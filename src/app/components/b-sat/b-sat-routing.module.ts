import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ELearningComponent } from './e-learning/e-learning.component';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { ExamComponent } from './exam/exam.component';
import { ProfileComponent } from './profile/profile.component';
import { MockTestComponent } from './mock-test/mock-test.component';
import { ReloadGuard } from 'src/app/shared/guard/reload.guard';
import { MockTestPageComponent } from './mock-test-page/mock-test-page.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'e-learning',
        component: ELearningComponent,
        data: {
          title: "E-learning",
          breadcrumb: "E-learning",
        }
      },
      {
        path: 'study-material',
        component: StudyMaterialComponent,
        data: {
          title: "Study Material",
          breadcrumb: "Study Material",
        }
      },
      {
        path: 'mock-test-page',
        component: MockTestPageComponent,
        data: {
          title: "Mock Test",
          breadcrumb: "Mock Test",
        }
      },
      {
        path: 'exam',
        component: ExamComponent,
        data: {
          title: "Exam",
          breadcrumb: "B-Sat Exam",
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: "Student Profile",
          breadcrumb: "Profile",
        }
      },
      {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
        data: {
          title: "Privacy Policy",
          breadcrumb: "Privacy Policy",
        }
      },
      {
        path: 'mock-test',
        component: MockTestComponent,
        canDeactivate: [ReloadGuard],
        data: {
          title: "Mock Test",
          breadcrumb: "Mock Test",
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BSatRoutingModule { }
