import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  studentDetails: any;
  email: any;
  isEditMode: boolean = false;
  userId: any;
  userEmail: any;

  constructor(private fb: FormBuilder, private httpService : HttpService) { }

  ngOnInit() {
    this.studentDetails = this.httpService.getUserDetails().data;
    console.log('this.studentDetails', this.studentDetails);

    // Set default value for 'email' based on 'getUserDetails().data.Email'
    this.email = this.studentDetails.email;
    this.userId = this.httpService.getUserId();
  }

  enableEditMode() {
    this.isEditMode = true;
  }
  
  disableEditMode() {
    this.isEditMode = false;
    // Reset email to the original value if the user cancels editing
    this.email = this.studentDetails.email;
  }

  updateEmail() {
    let sendData = {
      email: this.studentDetails.email,
    };
  
    this.httpService.put("studentRegistration/updateStudentRegistration", this.userId, sendData).subscribe({
      next: async (response) => {
        console.log('response', response);
        this.userEmail = response;
        this.isEditMode = false; 
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }
  toggleEditMode() {
    this.isEditMode = !this.isEditMode;
  }
}
