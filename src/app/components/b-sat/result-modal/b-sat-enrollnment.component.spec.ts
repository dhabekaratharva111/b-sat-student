import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BSatEnrollnmentComponent } from './b-sat-enrollnment.component';

describe('BSatEnrollnmentComponent', () => {
  let component: BSatEnrollnmentComponent;
  let fixture: ComponentFixture<BSatEnrollnmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BSatEnrollnmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BSatEnrollnmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
