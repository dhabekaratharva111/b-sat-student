import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-b-sat-enrollnment',
  templateUrl: './b-sat-enrollnment.component.html',
  styleUrls: ['./b-sat-enrollnment.component.scss']
})
export class BSatEnrollnmentComponent {
  receivedData?: any;

  constructor(public bsModalRef: BsModalRef, private location: Location) { }

  ngOnInit() {
    console.log('receivedData', this.receivedData)
  }

  okClick() {
    this.bsModalRef.hide()
    this.location.back();
  }
}
