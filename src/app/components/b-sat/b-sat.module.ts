import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BSatRoutingModule } from './b-sat-routing.module';
import { ELearningComponent } from './e-learning/e-learning.component';
import { ProfileComponent } from './profile/profile.component';
import { StudyMaterialComponent } from './study-material/study-material.component';
import { ExamComponent } from './exam/exam.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MockTestComponent } from './mock-test/mock-test.component';
import { SafePipe } from 'src/app/shared/pipes/safe.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { MockTestPageComponent } from './mock-test-page/mock-test-page.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';


@NgModule({
  declarations: [
    ELearningComponent,
    ProfileComponent,
    StudyMaterialComponent,
    ExamComponent,
    MockTestComponent,
    SafePipe,
    MockTestPageComponent,
    PrivacyPolicyComponent
  ],
  imports: [
    CommonModule,
    BSatRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class BSatModule { }
