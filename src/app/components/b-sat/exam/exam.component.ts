import { Component, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { Checkout } from 'capacitor-razorpay';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent {

  myClass: any;
  userId: any;
  enrollnmentDetails: any;
  bsatExamRegistrationId: any;
  examId: any;
  studentForm: FormGroup;
  uploadedImagesUrls: string[] = [];
  productImageSrc: any;
  modalRef?: BsModalRef;
  orderUserDetails = {
    name: 'Learn & Achiveve',
    description: 'Great offers',
    logo: 'https://ibb.co/1qBBz3V',
    prefill: {
      email: 'utkarsha.ioweb3@gmail.com',
      contact: '8308537209'
    }
  }
  orderCreatedData: any;
  examFeeValue: any;

  constructor(private router: Router, private location: Location, private httpService: HttpService,
    private http: HttpClient,
    private loaderService: LoaderService, private fb: FormBuilder, private modalService: BsModalService) {
  }

  ngOnInit() {
    this.studentForm = this.fb.group({
      examFee: ['', Validators.required],
      studentPhoto: ['', Validators.required],
      signaturePhoto: ['', Validators.required],
    });
    this.feesForScholership();

  }

  feesForScholership(): void {
    this.loaderService.startLoader();
    this.httpService.getById(`feesForScholership/getMyEnrollmentByClassId/${this.httpService.getMyClass()}`, this.httpService.getUserId()).subscribe(
      (response) => {
        this.enrollnmentDetails = response.data
        this.examFeeValue = response.data.examFee;
        console.log('this.examFeeValue', this.examFeeValue)
        this.examId = response?.data?._id;
        this.studentForm.get('examFee')?.setValue(this.examFeeValue);
        this.loaderService.stopLoader();
      },
      (error) => {
        console.error('API Error:', error);
      }
    );
  }

  createStudentImage(event: any, item: any): void {
    console.log('item', item)
    if (item == 'signaturePhoto') {
      const files = event.target.files;

      if (files && files.length > 0) {
        const formData = new FormData();

        // Append each file to the FormData
        for (const file of files) {
          formData.append('img', file, file.name);
        }

        // Send the FormData to the multiple upload image API
        this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)
          .subscribe(
            (response: any) => {
              console.log('Upload successful:', response);
              this.uploadedImagesUrls = response.data.img;
              console.log('this.@@@@@@@', this.uploadedImagesUrls)
              this.studentForm.get('signaturePhoto')?.patchValue(this.uploadedImagesUrls)
            },
            (error) => {
              console.error('Upload error:', error);
            }
          );
      }
    } else {
      const files = event.target.files;

      if (files && files.length > 0) {
        const formData = new FormData();

        // Append each file to the FormData
        for (const file of files) {
          formData.append('img', file, file.name);
        }

        // Send the FormData to the multiple upload image API
        this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)
          .subscribe(
            (response: any) => {
              console.log('Upload successful:', response);
              this.uploadedImagesUrls = response.data.img;
              console.log('this.@@@@@@@', this.uploadedImagesUrls)

              this.productImageSrc = this.uploadedImagesUrls;
              this.studentForm.get('studentPhoto')?.patchValue(this.uploadedImagesUrls)

            },
            (error) => {
              console.error('Upload error:', error);
            }
          );
      }
    }

  }

  // Step 1
  proceedPayment() {
    console.log('this.studentForm.value', this.studentForm.value)
    let sendData = {
      amount: this.studentForm.value.examFee,
      currency: "INR",
      receipt: "order_receipt",
    };

    this.httpService.post("registrationForScholershipPayment/proceedPayment", sendData).subscribe({
      next: async (response) => {
        // let confirmed: any = await this.alertService.showConfirmation('Subscribe Now!', `You are paying ${response.currency} ${response.amount / 100}.`, 'No', 'Pay Now !');
        // if (confirmed) {
        this.payWithRazorpay(response)
        // }
        this.studentForm.reset();
        this.modalRef?.hide();
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  // Step 2
  async payWithRazorpay(res: any) {
    const options = {
      key: 'rzp_test_ZkWbOZ5oCWVi6l',
      amount: res.amount,
      description: this.orderUserDetails.description,
      image: this.orderUserDetails.logo,
      order_id: res.id, //Order ID generated in Step 1
      currency: res.currency, //currency generated in Step 1
      name: this.orderUserDetails.name,
      prefill: {
        email: this.orderUserDetails.prefill.email,
        contact: this.orderUserDetails.prefill.contact
      },
      theme: {
        color: '#0029F5'
      }
    }

    try {
      let razorpaySuccessRes = (await Checkout.open(options));
      this.proceedToCreateOrder(razorpaySuccessRes.response);
      // Step 3 to create entry of order on success
    } catch (error: any) {
      console.log('######### Step:2 Error', error)
      // Step 3 to create entry of order on failure
    }
  }

  // step3
  proceedToCreateOrder(razorpaySuccessRes: any) {
    //data.response - This is response came from the razeorpay after payment success
    // Append your checkout data and make enrty in DB

    let sendData = {
      razorpayResData: razorpaySuccessRes,
      paymentStatus: 'paid',
      orderStatus: 'Pending',
      totalAmount: this.examFeeValue,
      bsatExamRegistrationId: this.examId,
      userId: this.httpService.getUserId(),
    }
    this.httpService.post("registrationForScholershipPayment/createRegistrationForScholershipPayment", sendData).subscribe({
      next: async (response) => {
        this.orderCreatedData = response
        // reloadApp
        window.location.reload();
        console.log('this.orderCreatedData', this.orderCreatedData);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  goToStudymaterial() {
    this.router.navigate(['/b-sat/study-material'])
  }

  openModal(template: TemplateRef<void>) {
    this.modalRef = this.modalService.show(template);
  }

  openNewWindow() {
    const profileWindow = window.open('http://localhost:4200/bsat-exam', '_blank', 'resizable=yes,scrollbars=yes');
    // Add event listener for the beforeunload event
    if (profileWindow) {

      profileWindow.onbeforeunload = (event) => {
        const confirmationMessage = 'Are you sure you want to leave? Your progress may be lost.';
        event.returnValue = confirmationMessage;
        return confirmationMessage;
      };
    }
  }
}
