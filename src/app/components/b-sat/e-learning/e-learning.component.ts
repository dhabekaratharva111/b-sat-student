import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { id } from 'date-fns/esm/locale';
import CommonConstants from 'src/app/shared/constants/global.const';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-e-learning',
  templateUrl: './e-learning.component.html',
  styleUrls: ['./e-learning.component.scss']
})
export class ELearningComponent {
  videoURL = "https://www.youtube.com/embed/LFoz8ZJWmPs";
  myClass: any;
  myMedium: any;
  classes = CommonConstants.CLASSES;
  medium = CommonConstants.MEDIUM;
  getYoutubeLinks: any = [];

  constructor(private fb: FormBuilder, private httpService: HttpService, 
    private loaderService : LoaderService,
    private sanitizer: DomSanitizer, private storageService: StorageService) { }

  ngOnInit() {
    this.myClass = this.httpService.getMyClass();
    this.myMedium = this.httpService.getMyMedium();
    console.log('########## this.myClass', this.myClass)
    console.log('########### this.myMedium', this.myMedium)
    this.loadYoutubeLinks()
  }

  loadYoutubeLinks() {
    this.loaderService.startLoader();
    this.httpService.getById(`addElerning/getYoutubeVideoByClassId/${this.myClass}`, this.myMedium).subscribe(
      (response) => {
        this.getYoutubeLinks = response.data;
        console.log('this.getYoutubeLinks', this.getYoutubeLinks);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      },
      (error) => {
        console.error('API Error:', error);
        setTimeout(() => {
          this.loaderService.stopLoader();
      }, 200);
      }
    );
  }

  getSafeVideoLink(videoLink: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(videoLink);
  }

  getVideoId(videoLink: string): string {
    return 'video_id';
  }
}
