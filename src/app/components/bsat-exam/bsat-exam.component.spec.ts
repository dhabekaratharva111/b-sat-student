import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsatExamComponent } from './bsat-exam.component';

describe('BsatExamComponent', () => {
  let component: BsatExamComponent;
  let fixture: ComponentFixture<BsatExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsatExamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BsatExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
