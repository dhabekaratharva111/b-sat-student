import { Component, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { ReloadPreventionService } from 'src/app/shared/services/reload/reload.service';
import { Location } from '@angular/common';

const LETTERS = ['A', 'B', 'C', 'D', 'E', 'F'];
@Component({
  selector: 'app-bsat-exam',
  templateUrl: './bsat-exam.component.html',
  styleUrls: ['./bsat-exam.component.scss']
})
export class BsatExamComponent {
  optionLetters = LETTERS;
  selectedTestId: any;
  questions: any[] = []; // Update the type here
  currentQuestionIndex: number = 0;
  selectedOptionIndex: number | null = null; // Change the name here
  testId: string = '';
  getResultById: any;
  questionListByTest: any;
  selectedQuestionIndex: any = 0;
  selectedQuestion: any;
  modalRef?: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false  // Set to false to disable closing with the "Escape" key
  };
  receivedData: any;
  testDuration: any;
  remainingTime: number;
  formattedRemainingTime: string;
  timerInterval: any;
  @ViewChild('resulttemplate', { static: true }) resulttemplate: TemplateRef<any>;
  hasUnsavedChanges: any = true;


  constructor(
    private location: Location,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute,
    private httpService: HttpService,
    private reloadPreventionService: ReloadPreventionService
  ) {
  }

  ngOnInit() {
    this.reloadPreventionService.preventPageReload(true);

    this.route.queryParams.subscribe(params => {
      this.testId = params['testID'];
      this.testDuration = params['testTime'];
      console.log('test', this.testDuration)
      if (this.testId) { 
        this.loadMockTestQuestionsById();
      }
    });

    this.remainingTime = this.testDuration * 60;
    this.startTimer();
  }

  loadMockTestQuestionsById() {
    this.httpService.getById('mockTestQuestions/getMockTestQuestionByMockTestId', this.testId).subscribe(
      (response) => {
        this.questionListByTest = response.data;
        this.onSelectQuestion(this.questionListByTest[this.selectedQuestionIndex], this.selectedQuestionIndex);
        // this.iAmNew =false;
      },
      (error) => {
        console.error('API Error:', error);
      }
    );
  }

  onSelectQuestion(question: object, index: any) {
    this.selectedQuestion = question;
    this.selectedQuestionIndex = index;
  }

  onSelectAnswer(optionId: any) {
    const selectedQuestionId = this.selectedQuestion._id;

    const selectedQuestion = this.questionListByTest.find((que: any) => que._id === selectedQuestionId);

    if (selectedQuestion) {
      selectedQuestion.answerId = optionId;

      selectedQuestion.optionList.forEach((option: any) => {
        option.isSelected = option.optionId === optionId;
      });
    }
  }

  onClickNext() {
    this.selectedQuestionIndex = this.selectedQuestionIndex + 1;
    this.onSelectQuestion(this.questionListByTest[this.selectedQuestionIndex], this.selectedQuestionIndex)
  }

  onClickPrev() {
    this.selectedQuestionIndex = this.selectedQuestionIndex - 1;
    this.onSelectQuestion(this.questionListByTest[this.selectedQuestionIndex], this.selectedQuestionIndex)
  }

  onSubmitTest(resulttemplate: TemplateRef<void>) {
    let sendData = {
      "mockTestId": this.questionListByTest[0].mockTestId,
      "userId": this.httpService.getUserId(),
      "answerSheet": this.getFormatedAnswer()
    }

    this.httpService.post('submitmockTest/submitMockTest', sendData).subscribe(
      (res) => {
        this.receivedData = res.data;
        this.openResultModal(resulttemplate);
      },
      (error) => {
        console.log('error', error);
      });

  }

  getFormatedAnswer() {
    return this.questionListByTest
      .filter((element: any) => element.answerId !== undefined && element.answerId !== null)
      .map((element: any) => ({
        questionId: element._id,
        answerId: element.answerId,
      }));
  }

  openResultModal(resulttemplate: any) {
    this.modalService.show(resulttemplate, this.config);
  }

  okClick() {
    this.modalRef?.hide();
    this.location.back();
    this.hasUnsavedChanges = false;
    this.reloadPreventionService.preventPageReload(false);
  }

  startTimer() {
    this.timerInterval = setInterval(() => {
      this.remainingTime--;
      this.formattedRemainingTime = this.formatTimeDuration(this.remainingTime);
      if (this.remainingTime <= 0) {
        clearInterval(this.timerInterval);
        this.onSubmitTest(this.resulttemplate);
      }
    }, 1000);
  }

  private formatTimeDuration(durationInSeconds: number): string {
    const hours = Math.floor(durationInSeconds / 3600);
    const minutes = Math.floor((durationInSeconds % 3600) / 60);
    const seconds = durationInSeconds % 60;

    return `${this.padZero(hours)} : ${this.padZero(minutes)} : ${this.padZero(seconds)}`;
  }

  private padZero(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }


  canDeactivate(): boolean {
    if (this.hasUnsavedChanges) {
      return window.confirm('Do you really want to leave?');
    } else {
      return true;

    }
  };



}
