import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ReloadPreventionService {
  private preventReload = false;

  constructor(private router: Router) {
    window.addEventListener('beforeunload', (event: any): any => {
      if (this.preventReload) {
        const message = 'You have unsaved changes. Are you sure you want to leave?';
        (event as any).returnValue = message; 
        return message;
      }
    });
  }

  preventPageReload(flag: any) {
    this.preventReload = flag;
  }

}
