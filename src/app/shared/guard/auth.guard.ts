import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { AuthenticationService } from "../services/authentication.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthenticationService, private router: Router) {}

  canActivate(): boolean {
    this.authService.isAuthenticated.subscribe((authenticated) => {
      if (authenticated) {
        return true;
      } else {
        this.router.navigate(["auth/login"]);
        return false;
      }
    });
    return true;
  }
}
