import { Component } from '@angular/core';
import { HttpService } from '../../services/http/http.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { Checkout } from 'capacitor-razorpay';
declare var Razorpay: any;


@Component({
  selector: 'app-payment-gate',
  templateUrl: './payment-gate.component.html',
  styleUrls: ['./payment-gate.component.scss']
})
export class PaymentGateComponent {
   orderUserDetails = {
    name: 'Best Agro',
    description: 'Great offers',
    logo: 'https://ibb.co/1qBBz3V',
    prefill: {
      email: 'utkarsha.ioweb3@gmail.com',
      contact: '8308537209'
    }
  }
  orderCreatedData: any;

  constructor(
    // private commonService: CommonService,
    private httpService: HttpService,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit() {

  }

  proceedPayment() {
    let sendData = {
      amount: 200,
      currency: "INR",
      receipt: "order_receipt", //orderId,
    };

    this.httpService.post("registrationForScholershipPayment/proceedPayment", sendData).subscribe({
      next: async (response) => {
        // let confirmed: any = await this.alertService.showConfirmation('Subscribe Now!', `You are paying ${response.currency} ${response.amount / 100}.`, 'No', 'Pay Now !');
        // if (confirmed) {
          this.payWithRazorpay(response)
        // }
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  // Step 2
  async payWithRazorpay(res: any) {
    const options = {
      key: 'rzp_test_ZkWbOZ5oCWVi6l',
      amount: res.amount,
      description: this.orderUserDetails.description,
      image: this.orderUserDetails.logo,
      order_id: res.id, //Order ID generated in Step 1
      currency: res.currency, //currency generated in Step 1
      name: this.orderUserDetails.name,
      prefill: {
        email: this.orderUserDetails.prefill.email,
        contact: this.orderUserDetails.prefill.contact
      },
      theme: {
        color: '#0029F5'
      }
    }

    try {
      let razorpaySuccessRes = (await Checkout.open(options));
      this.proceedToCreateOrder(razorpaySuccessRes.response);
      // Step 3 to create entry of order on success
    } catch (error: any) {
      console.log('######### Step:2 Error', error)
      // Step 3 to create entry of order on failure
    }
  }

  // step3

  proceedToCreateOrder(razorpaySuccessRes: any) {
    //data.response - This is response came from the razeorpay after payment success
    // Append your checkout data and make enrty in DB

    let sendData = {
      razorpayResData: razorpaySuccessRes,
      paymentStatus: 'paid',
      orderStatus: 'Pending',
      totalAmount: 200,
      userId: this.httpService.getUserId(),
      payFee:{
        "classId":"2",
        "examFee":"800",
        "userImg":"http://localhost:8080/assets/uploadImg/1702359303815-maharaj.jpg",
        "userSignImg":"http://localhost:8080/assets/uploadImg/1702359303815-maharaj.jpg",
    },
    }
    this.httpService.post("registrationForScholershipPayment/createRegistrationForScholershipPayment", sendData).subscribe({
      next: async (response) => {
        this.orderCreatedData = response
        console.log('this.orderCreatedData', this.orderCreatedData);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }


}
