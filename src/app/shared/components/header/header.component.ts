import { Component, ElementRef, OnInit, TemplateRef } from '@angular/core';
import { NavService } from 'src/app/shared/services/nav/nav.service';
import { AuthenticationService } from '../../services/authentication.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../../services/http/http.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../services/storage.service';
import { TokenService } from '../../services/token.service';
import { Checkout } from 'capacitor-razorpay';
import { Subscription, interval } from 'rxjs';
declare var Razorpay: any;
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  orderUserDetails = {
    name: 'Learn & Achiveve',
    description: 'Great offers',
    logo: 'https://ibb.co/1qBBz3V',
    prefill: {
      email: 'utkarsha.ioweb3@gmail.com',
      contact: '8308537209'
    }
  }
  studentForm: FormGroup;
  modalRef?: BsModalRef;
  collapseSidebar: boolean = true;
  uploadedImagesUrls: string[] = [];
  uploadedImageUrl: any;
  uploadedImageUrl1: string | null = null;
  productImageSrc: any;
  planDetails: any = [];
  isTimerVisible: boolean = false;
  countdownText: string = '';
  staticDateTime: Date = new Date(2023, 11, 15, 14, 0); // December 15, 2023, 2:00 PM
  countdownInterval: Subscription | undefined;
  orderCreatedData: any;
  companyName = "Leran & Achieve";
  RegistrationForScholership: any;
  userId: any;
  examId: any;
  examFeeValue: any;
  constructor(private navServices: NavService, private http: HttpClient,
    private elementRef: ElementRef, private authenticationService: AuthenticationService, private modalService: BsModalService, private fb: FormBuilder,
    private httpService: HttpService, private storageService: StorageService,
    private tokenService: TokenService,private location: Location
    ) {
      
      console.log('examFeeValue', this.examFeeValue)
  }
  open = false;

  sidebarToggle() {
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }
 
  ngOnInit(): void {
    this.studentForm = this.fb.group({
      examFee: ['', Validators.required],
      studentPhoto: ['', Validators.required],
      signaturePhoto: ['', Validators.required],
    });


    this.userId = this.httpService.getUserId();
    this.feesForScholership()
    console.log('this.userId', this.userId);
    // Start the countdown
    this.startCountdown();
  }


  ngOnDestroy() {
    this.stopCountdown();
  }
  
  private startCountdown() {
    this.countdownInterval = interval(1000).subscribe(() => {
      const currentTime = new Date();
      const timeDifference = this.staticDateTime.getTime() - currentTime.getTime();

      if (timeDifference > 0 && timeDifference <= 5 * 60 * 1000) {
        this.isTimerVisible = true;
        const minutes = Math.floor(timeDifference / (1000 * 60));
        const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);
        this.countdownText = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
      } else {
        this.isTimerVisible = false;
        this.countdownText = '';
      }
    });
  }

  private stopCountdown() {
    if (this.countdownInterval) {
      this.countdownInterval.unsubscribe();
    }
  }

  buttonClick() {
    console.log('Button clicked!');
  }
  openMenu() {
    this.open = !this.open
  }

  languageToggle() {
    this.navServices.language = !this.navServices.language;
  }
  openModal(template: TemplateRef<void>) {
    this.modalRef = this.modalService.show(template);
  }

  createStudentImage(event: any, item: any): void {
    console.log('item', item)
    if (item == 'signaturePhoto') {
      const files = event.target.files;

      if (files && files.length > 0) {
        const formData = new FormData();

        // Append each file to the FormData
        for (const file of files) {
          formData.append('img', file, file.name);
        }

        // Send the FormData to the multiple upload image API
        this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)
          .subscribe(
            (response: any) => {
              console.log('Upload successful:', response);
              this.uploadedImagesUrls = response.data.img;
              console.log('this.@@@@@@@', this.uploadedImagesUrls)
              this.studentForm.get('signaturePhoto')?.patchValue(this.uploadedImagesUrls)
            },
            (error) => {
              console.error('Upload error:', error);
            }
          );
      }
    } else {
      const files = event.target.files;

      if (files && files.length > 0) {
        const formData = new FormData();

        // Append each file to the FormData
        for (const file of files) {
          formData.append('img', file, file.name);
        }

        // Send the FormData to the multiple upload image API
        this.http.post('https://bsat.onrender.com/uploadImage/createUploadImage', formData)
          .subscribe(
            (response: any) => {
              console.log('Upload successful:', response);
              this.uploadedImagesUrls = response.data.img;
              console.log('this.@@@@@@@', this.uploadedImagesUrls)

              this.productImageSrc = this.uploadedImagesUrls;
              this.studentForm.get('studentPhoto')?.patchValue(this.uploadedImagesUrls)

            },
            (error) => {
              console.error('Upload error:', error);
            }
          );
      }
    }

  }


  feesForScholership(): void {
    this.httpService.getById(`feesForScholership/getMyEnrollmentByClassId/${this.httpService.getMyClass()}`, this.httpService.getUserId()).subscribe(
      (response) => {
        // this.classFee = response.data;
        this.examFeeValue = response.data.examFee;
        console.log('this.examFeeValue', this.examFeeValue)
        this.examId = response?.data?._id;
        // const formattedExamFee = `₹ ${examFeeValue}`;
        this.studentForm.get('examFee')?.setValue(this.examFeeValue);
      },
      (error) => {
        console.error('API Error:', error);
      }
    );
  }

  proceedPayment() {
    console.log('this.studentForm.value', this.studentForm.value)
    let sendData = {
      amount: this.studentForm.value.examFee,
      currency: "INR",
      receipt: "order_receipt",
    };

    this.httpService.post("registrationForScholershipPayment/proceedPayment", sendData).subscribe({
      next: async (response) => {
        // let confirmed: any = await this.alertService.showConfirmation('Subscribe Now!', `You are paying ${response.currency} ${response.amount / 100}.`, 'No', 'Pay Now !');
        // if (confirmed) {
        this.payWithRazorpay(response)
        // }
        this.studentForm.reset();
        this.modalRef?.hide();
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  // Step 2
  async payWithRazorpay(res: any) {
    const options = {
      key: 'rzp_test_ZkWbOZ5oCWVi6l',
      amount: res.amount,
      description: this.orderUserDetails.description,
      image: this.orderUserDetails.logo,
      order_id: res.id, //Order ID generated in Step 1
      currency: res.currency, //currency generated in Step 1
      name: this.orderUserDetails.name,
      prefill: {
        email: this.orderUserDetails.prefill.email,
        contact: this.orderUserDetails.prefill.contact
      },
      theme: {
        color: '#0029F5'
      }
    }

    try {
      let razorpaySuccessRes = (await Checkout.open(options));
      this.proceedToCreateOrder(razorpaySuccessRes.response);
      // Step 3 to create entry of order on success
    } catch (error: any) {
      console.log('######### Step:2 Error', error)
      // Step 3 to create entry of order on failure
    }
  }

  // step3

  proceedToCreateOrder(razorpaySuccessRes: any) {
    //data.response - This is response came from the razeorpay after payment success
    // Append your checkout data and make enrty in DB

    let sendData = {
      razorpayResData: razorpaySuccessRes,
      paymentStatus: 'paid',
      orderStatus: 'Pending',
      totalAmount: this.examFeeValue,
      bsatExamRegistrationId: this.examId,
      userId: this.httpService.getUserId(),
    }
    this.httpService.post("registrationForScholershipPayment/createRegistrationForScholershipPayment", sendData).subscribe({
      next: async (response) => {
        this.orderCreatedData = response
        // reloadApp
        window.location.reload();
        console.log('this.orderCreatedData', this.orderCreatedData);
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  logout() {
    this.authenticationService.logout()
  }

}
