import { Component, Input, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/shared/services/loader.service';

@Component({
  selector: 'app-loader-design',
  templateUrl: './loader-design.component.html',
  styleUrls: ['./loader-design.component.scss'],
})
export class LoaderDesignComponent  implements OnInit {
  @Input() text: string = 'Loading...';

  constructor(public loaderService: LoaderService) { }

  ngOnInit() {}

}
