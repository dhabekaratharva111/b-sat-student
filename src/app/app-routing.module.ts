import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ContentComponent } from './shared/components/layout/content/content.component';
import { AdminGuard } from './shared/guard/admin.guard';
import { content } from './shared/routes/routes';
import { full } from './shared/routes/full.routes';
import { FullComponent } from './shared/components/layout/full/full.component';
import { AutoLoginGuard } from './shared/guard/auto-login.guard';
import { AuthGuard } from './shared/guard/auth.guard';
import { MockTestComponent } from './components/b-sat/mock-test/mock-test.component';
import { BsatExamComponent } from './components/bsat-exam/bsat-exam.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/b-sat/e-learning',
    pathMatch: 'full'
  },
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
    canActivate: [AutoLoginGuard],
  },
  {
    path: 'bsat-exam',
    component: BsatExamComponent,
  },
  {
    path: '',
    component: ContentComponent,
    canActivate: [AuthGuard],
    children: content
  },
  {
    path: '',
    component: FullComponent,
    canActivate: [AuthGuard],
    children: full
  },
  {
    path: '**',
    redirectTo: ''
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
