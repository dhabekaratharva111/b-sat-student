import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavigationExtras, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import CommonConstants from "src/app/shared/constants/global.const";
import { AuthenticationService } from "src/app/shared/services/authentication.service";
import { HttpService } from "src/app/shared/services/http/http.service";
import { LoaderService } from "src/app/shared/services/loader.service";
import { StorageService } from "src/app/shared/services/storage.service";
import { TokenService } from "src/app/shared/services/token.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {

    public show: boolean = false;
    public loginForm: FormGroup;
    public errorMessage: any;
    modalRef?: BsModalRef;
    forgetForm: FormGroup

    constructor(private fb: FormBuilder, public router: Router,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService,
        private storageService: StorageService,
        private tokenService: TokenService,
        private loaderService : LoaderService,
        private modalService: BsModalService,
        private httpService: HttpService) {
        this.loginForm = this.fb.group({
            userId: ["", [Validators.required, Validators.email]],
            password: ["", Validators.required],
        });
        this.forgetForm = this.fb.group({
            mobileNo: ["", Validators.required],
        });
    }

    ngOnInit() { }

    showPassword() {
        this.show = !this.show;
    }

    login(): void {
      this.loaderService.startLoader();
        this.authenticationService.login(this.loginForm.value.userId, this.loginForm.value.password).subscribe({
            next: (response) => {
                this.router.navigate(["/b-sat/e-learning"]);
                this.tokenService.setToken(response.token);
                this.storageService.setItem(CommonConstants.USER_DETAILS_KEY, JSON.stringify(response));
                this.authenticationService.isAuthenticated.next(true);
                this.toastr.success('Login successfully..!');
                setTimeout(() => {
                  this.loaderService.stopLoader();
              }, 200);
            },
            error: (err) => {
                console.log("err", err);
                this.toastr.error(err?.error?.messages.error); 
                setTimeout(() => {
                  this.loaderService.stopLoader();
              }, 200);
            },
            complete: () => {
                console.info("complete.!!");
            },
        });
    }

    generateOTP() {
        let dataToSend = {
          mobileNo: '91' + this.forgetForm.value.mobileNo
        };
      
        this.httpService.post("studentRegistration/forgetPassword", dataToSend).subscribe({
          next: (response) => {
            console.log('response', response);
      
            // Combine mobileNo and response into a single object
            let queryParams = {
              mobileNo: dataToSend.mobileNo,
              responseData: response
            };
      
            let objToSend: NavigationExtras = {
              queryParams: queryParams,
              skipLocationChange: false,
              fragment: 'top'
            };
      
            this.router.navigate(['auth/otpVerify'], { state: objToSend });
            this.forgetForm.reset();
            this.modalRef?.hide();
          },
          error: (err) => {
            console.log("err", err);
            this.toastr.error(err?.error?.messages.error); // Fallback message
          },
          complete: () => {
            console.info("complete.!!");
          },
        });
      }
      

    openModal(template: TemplateRef<void>) {
        this.modalRef = this.modalService.show(template);
    }

    submitForm() {

    }
}